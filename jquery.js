// $('body').append('a', href="Most popular posts").text('Most popular posts').addClass('foto-title')
// $('body').append('a', href="Top rated").text('Top rated').addClass('popular-title')
// $('body').append('a', href="hot news").text('hot news').addClass('news-title')
// $('a').wrapAll('div')


$(document).ready(function(){                                                       //Запустить код, когда он будет загружен

    $('a.scrollto').click(function(event){                                          // при клике на ссылку, вызываем функцыю
        const elementClick = $(this).attr("href")                                   // (устанавливаем или) возвращаем значение атрибута
        const destination = $(elementClick).offset().top                            // получить текущую позицию элемента относительно документа 
        $("html, body").animate({scrollTop: destination}, 1800)                     // html и body оборачиваем в jQuery,и функция будет работать на оба елемента. создаём анимацию, которая будет работать с аргументом объект : свойство которое хотим анимировать
        event.preventDefault()                                                      // отмены действия браузера нажатия по ссылке
    })

    $('#up').click(function(){                                         // при клике на кнопку 
        $([document.body, document.documentElement]).animate({         
            scrollTop: 0                                               // анимация поднимется вверх 
        }, 1800)
    })

    $(window).scroll(function(){                               // на window указываем scrollTop прокрутку вверх
        if($(window).scrollTop() > $(window).height()){        // если scrollTop больше высоты страницы
            $('#up').show()                                    // показать кнопку
        } else {                                               // иначе 
            $('#up').hide()                                    // скрыть кнопку
        }
    })

    $('.slideToggle').click(function(){                       // при клике на кнопку с классом slideToggle выполниться
    $('.foto-header').slideToggle()                           // скрить или показать секцию с классом foto-header
    })
})




